NextCloud Kustomization
========================

Launches a single-node NextCloud server storing the data locally.

OCC command
===========

To run the `occ` command after upgrades or for other maintenance work, a run container can be deployed by hand.

```
kubectl apply -f run.yaml
```

Wait for the pod to start and then use `kubectl exec` to access it. It might be necessary to start /cron.sh to get all nextcloud files into /var/www/html.
